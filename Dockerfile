FROM cloudron/base:0.10.0

ENV PATH /usr/local/node-4.7.3/bin:$PATH

RUN mkdir -p /app/code
WORKDIR /app/code

# RUN git clone https://github.com/eclipse/che.git && cd che && git checkout tags/6.9.0

# https://github.com/eclipse/che/wiki/Development-Workflow#build-and-run
# RUN cd /app/code/che/assembly && mvn clean install -Pfast

RUN curl -sL https://download.docker.com/linux/ubuntu/dists/xenial/pool/stable/amd64/docker-ce_18.03.1~ce-0~ubuntu_amd64.deb -o /tmp/docker.deb
RUN apt-get update && apt install -y /tmp/docker.deb

# RUN npm install -g bower gulp
# RUN echo "{ \"allow_root\": true }" > "/root/.bowerrc"
# RUN cd /app/code/che/dashboard && mvn -e -X clean install

COPY start.sh nginx.conf /app/code/

CMD [ "/app/code/start.sh" ]