#!/bin/bash

set -eu

echo "=> Start che containers"
# TODO this needs to be multi app aware
CHE_HOST_CONTAINER_ID="`docker run -d -p 8080:8080 --name che --rm -e CHE_LOG_LEVEL=info -e DOCKER_HOST=${DOCKER_HOST} -e CHE_HOST=${APP_DOMAIN} -v /app/data/data:/data eclipse/che-server:6.9.0`"

echo "=> Configure nginx reverse proxy"
CHE_HOST_CONTAINER_IP="`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CHE_HOST_CONTAINER_ID}`"
sed -e "s,##CHE_HOST_CONTAINER_IP##,${CHE_HOST_CONTAINER_IP}," /app/code/nginx.conf > /run/nginx.conf

echo "=> Start nginx"
nginx -c /run/nginx.conf